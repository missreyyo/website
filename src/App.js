import "./App.css";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Home from "./pages/Home";
import Menu from "./pages/Menu";
import About from "./pages/About";
import Contact from "./pages/Contact";
import VoteFunc from "./pages/Vote";
import Products from "./pages/Products";
import Game from "./pages/Game";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/menu" exact component={Menu} />
          <Route path="/about" exact component={About} />
          <Route path="/contact" exact component={Contact} />
          <Route
            path="/products/vote/0x21bda6651CE7170FDB1c0b77ecEA764dEA61D5Ff/0xE7522E2Ae6C375F8D7186267D0858CaC897a1850"
            exact
            component={VoteFunc}
          />
          <Route
            path="/products/vote/0x16875dc47AE7738904F218CD6Fd7B02d70dda6F5/0x9C1DbFAa4c6480Bc59774643EED01d1b8D0B0F15"
            exact
            component={VoteFunc}
          />
          <Route path="/products" exact component={Products} />
          <Route path="/game" exact component={Game} />
        </Switch>
        <Footer />
      </Router>
    </div>
  );
}

export default App;
