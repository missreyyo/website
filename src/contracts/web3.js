import Web3 from "web3";
import env from "./env.json";

let web3;

if (typeof window !== "undefined" && typeof window.ethereum !== "undefined") {
  //window.ethereum.request({ method: "eth_requestAccounts" });
  web3 = new Web3(window.ethereum);
} else {
  web3 = new Web3(new Web3.providers.HttpProvider(env.infuraAPI));
}

export default web3;
