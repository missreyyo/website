import React from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

function CupertinoButtonDanger(props) {
  return (
    <Link to={props.to}>
      <Container {...props}>
        <Caption>{props.caption || "Button"}</Caption>
      </Container>
    </Link>
  );
}

const Container = styled.div`
  display: flex;
  background-color: #FF3B30;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  border-radius: 5px;
  padding-left: 16px;
  padding-right: 16px;
`;

const Caption = styled.span`
  font-family: Roboto;
  color: #fff;
  font-size: 17px;
  font-weight: 500;
`;

export default CupertinoButtonDanger;
