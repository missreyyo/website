import React from "react";
import styled from "styled-components";

function CupertinoButton(props) {
  return (
      <Container {...props}>
        <Caption>{props.caption || "Button"}</Caption>
      </Container>
  );
}

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  border-radius: 5px;
  padding-left: 16px;
  padding-right: 16px;
  cursor: pointer;
`;

const Caption = styled.span`
  font-family: Roboto;
  color: #fff;
  font-size: 17px;
  font-weight: 500;
`;

export default CupertinoButton;
