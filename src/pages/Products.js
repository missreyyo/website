import React from "react";
import styled from "styled-components";
import CupertinoButtonDanger from "../components/CupertinoButtonDanger";
import tren from "../assets/vote/tren.png";
import robot from "../assets/vote/robot.png";
import vote from "../assets/vote/vote.png";

function Products() {
  return (
    <>
      <HeaderColumnRow>
        <HeaderColumn>
          <Header>Oylanabilir Ürünler</Header>
          <ProductsRow>
            <Product1Img src={tren}></Product1Img>
            <Product2Img src={robot}></Product2Img>
          </ProductsRow>
        </HeaderColumn>
        <VoteImg src={vote}></VoteImg>
      </HeaderColumnRow>
      <CupertinoButtonDangerRow>
        <CupertinoButtonDanger
          caption="Oyla"
          style={{
            height: 66,
            width: 376,
          }}
          to="products/vote/0x21bda6651CE7170FDB1c0b77ecEA764dEA61D5Ff/0xE7522E2Ae6C375F8D7186267D0858CaC897a1850"
        ></CupertinoButtonDanger>
        <CupertinoButtonDanger
          caption="Oyla"
          style={{
            height: 66,
            width: 362,
            marginLeft: 50,
          }}
          to="products/vote/0x16875dc47AE7738904F218CD6Fd7B02d70dda6F5/0x9C1DbFAa4c6480Bc59774643EED01d1b8D0B0F15"
        ></CupertinoButtonDanger>
      </CupertinoButtonDangerRow>
    </>
  );
}

const HeaderColumnRow = styled.div`
  flex-direction: row;
  display: flex;
  margin-left: 300px;
`;

const HeaderColumn = styled.div`
  flex-direction: column;
  display: flex;
  margin-top: 50px;
`;

const Header = styled.span`
  width: 600px;
  height: 100px;
  margin-left: 200px;
  font-family: Courier;
  font-style: normal;
  font-weight: 800;
  color: rgba(150, 34, 34, 1);
  font-size: 50px;
  text-align: center;
`;

const ProductsRow = styled.div`
  flex-direction: row;
  display: flex;
  //margin-top: 89px;
  //margin-left: 100px;
`;

const Product1Img = styled.img`
  width: 100%;
  height: 200px;
  margin-top: 54px;
  margin-left: 90px;
  object-fit: contain;
`;

const Product2Img = styled.img`
  width: 100%;
  height: 300px;
  margin-left: 100px;
  object-fit: contain;
`;

const VoteImg = styled.img`
  width: 100%;
  height: 300px;
  object-fit: contain;
  margin-left: 200px;
`;

const CupertinoButtonDangerRow = styled.div`
  height: 66px;
  flex-direction: row;
  display: flex;
  //margin-top: 709px;
  margin-left: 400px;
  margin-right: 370px;
`;

export default Products;
