import React, { Component } from "react";
import token from "../contracts/mietoken";
import web3 from "../contracts/web3";
import styled from "styled-components";
import silver from "../assets/vote/silver.png";
import gold from "../assets/vote/gold.png";
import diamond from "../assets/vote/diamond.png";
import platinum from "../assets/vote/platinum.png";
import frameImg from "../assets/vote/frame.png";
import voteImg from "../assets/vote/vote.png";
import CupertinoButton from "../components/CupertinoButton";
import getDaoVote from "../contracts/daovote";

function VoteFunc(prob) {
  const pathnameSplit = prob.location.pathname.split("/");
  const daoVoteAddress = pathnameSplit[3];
  const developerAddress = pathnameSplit[4];

  return (
    <Vote daoVoteAddress={daoVoteAddress} developerAddress={developerAddress} />
  );
}

const ConnectWalletButtonRow = styled.div`
  height: 734px;
  flex-direction: row;
  display: flex;
  margin-left: 10px;
`;

const HeaderColumn = styled.div`
    width: 1080px;
    flex-direction: column
    display: flex;
    margin-top: 104px;
    margin-left: 16px;
  `;

const Header = styled.span`
  font-family: Courier;
  font-style: normal;
  font-weight: 400;
  color: rgba(150, 34, 34, 1);
  font-size: 103px;
  text-align: center;
  margin-left: 50px;
`;

const StarStackRow = styled.div`
  height: 448px;
  flex-direction: row;
  display: flex;
`;

const StarStack = styled.div`
  width: 270px;
  height: 448px;
  position: relative;
`;

const StarImage = styled.img`
  left: 35px;
  width: 200px;
  height: 200px;
  position: absolute;
  object-fit: contain;
`;

const Frame = styled.div`
  top: 195px;
  right: 20px;
  width: 270px;
  height: 280px;
  position: absolute;
  flex-direction: column;
  display: flex;
  background-image: url(${frameImg});
  background-size: cover;
`;

const Text = styled.span`
  font-family: Courier;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  font-size: 35px;
  margin-top: 120px;
  margin-left: 51px;
`;

const VoteColumn = styled.div`
  width: 412px;
  flex-direction: column;
  display: flex;
  margin-bottom: 248px;
`;

const VoteImg = styled.img`
  width: 300px;
  height: 100%;
  object-fit: contain;
  margin-left: 10px;
`;

const MessageArea = styled.div`
  width: 239px;
  height: 100px;
  background-color: #e6e6e6;
  flex-direction: column;
  display: flex;
  margin-top: 2px;
  margin-left: 61px;
`;

const Message = styled.span`
  font-family: Courier;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  width: 239px;
  height: 100px;
  line-height: 30px;
  font-size: 20px;
`;

const VoteButtonRow = styled.div`
  height: 60px;
  flex-direction: row;
  display: flex;
  margin-left: 145px;
`;

const VoteCountRow = styled.div`
  flex-direction: row;
  display: flex;
  margin-top: 5px;
  margin-left: 170px;
`;

const VoteCountStack = styled.div`
  width: 200px;
  height: 44px;
  position: relative;
`;

const VoteCountContainer = styled.div`
  width: 200px;
  height: 44px;
  background-color: #e6e6e6;
  flex-direction: column;
  display: flex;
`;

const VoteCountText = styled.span`
  font-family: Courier;
  top: 8px;
  left: 8px;
  position: absolute;
  font-style: normal;
  font-weight: 800;
  color: #121212;
  height: 44px;
  width: 200px;
  font-size: 20px;
  border: none;
  background: transparent;
`;

class Vote extends Component {
  state = {
    connectWalletText: "Cüzdanı Bağla",
    address: "",
    message: "Oylama Sonucu\nSonuçlandırılmadı",
    silver: 0,
    gold: 0,
    diamond: 0,
    platinum: 0,
  };

  count = 0;

  daoVote;

  initState = async () => {
    if (this.count === 0) {
      this.daoVote = getDaoVote(this.props.daoVoteAddress);
      await this.getVotes();
      this.count++;
    }
  };

  connectWallet = async () => {
    try {
      window.ethereum.request({ method: "eth_requestAccounts" });
      const accounts = await web3.eth.getAccounts();
      this.setState({ connectWalletText: "Bağlanıyor..." });
      this.setState({ address: accounts[0] });
      this.setState({ connectWalletText: "Cüzdan Bağlandı" });
    } catch (err) {
      const errStr = err.toString();
      if (errStr.includes("request")) {
        this.setState({ message: "Metamask kurulu değil" });
      } else {
        this.setState({ message: errStr });
      }
    }
  };

  getSilver = async () => {
    let tempSilver = await this.daoVote.methods.getSilver().call();
    this.setState({ silver: tempSilver });
  };

  getGold = async () => {
    let tempGold = await this.daoVote.methods.getGold().call();
    this.setState({ gold: tempGold });
  };

  getDiamond = async () => {
    let tmpDiamod = await this.daoVote.methods.getDiamond().call();
    this.setState({ diamond: tmpDiamod });
  };

  getPlatinum = async () => {
    let tmpPlatinum = await this.daoVote.methods.getPlatinum().call();
    this.setState({ platinum: tmpPlatinum });
  };

  voteSilver = async () => {
    if (this.state.address !== "") {
      this.setState({ message: "Gümüş paketine oy veriliyor" });

      await this.daoVote.methods.voteSilver().send({
        from: this.state.address,
        gasLimit: "100000",
      });

      await this.getSilver();

      this.setState({ message: "Gümüş paketine 1 oy verildi" });
    }
  };

  voteGold = async () => {
    if (this.state.address !== "") {
      this.setState({ message: "Altın paketine oy veriliyor" });

      await this.daoVote.methods.voteGold().send({
        from: this.state.address,
        gasLimit: "100000",
      });

      await this.getGold();

      this.setState({ message: "Altın paketine 1 oy verildi" });
    }
  };

  voteDiamond = async () => {
    if (this.state.address !== "") {
      this.setState({ message: "Elmas paketine oy veriliyor" });

      await this.daoVote.methods.voteDiamond().send({
        from: this.state.address,
        gasLimit: "100000",
      });

      await this.getDiamond();

      this.setState({ message: "Elmas paketine 1 oy verildi" });
    }
  };

  votePlatinum = async () => {
    if (this.state.address !== "") {
      this.setState({ message: "Platin paketine oy veriliyor" });

      await this.daoVote.methods.votePlatinum().send({
        from: this.state.address,
        gasLimit: "100000",
      });

      await this.getPlatinum();

      this.setState({ message: "Platin paketine 1 oy verildi" });
    }
  };

  finalizeVote = async () => {
    this.setState({ message: "Oylama Sonuçlandırılıyor" });

    try {
      let wonIndex = await this.daoVote.methods.findMax().call();
      await this.daoVote.methods.finalizeVote().send({
        from: this.state.address,
        gasLimit: "100000",
      });
      await this.getZeroVotes();
      let tmpMessage;
      switch (wonIndex) {
        case "0":
          tmpMessage = "Platin";
          break;
        case "1":
          tmpMessage = "Elmas";
          break;
        case "2":
          tmpMessage = "Altın";
          break;
        default:
          tmpMessage = "Gümüş";
      }
      this.setState({
        message:
          "Developer " +
          tmpMessage +
          " Paketi kazandı. Developera MiEToken transfer ediliyor",
      });
      await this.transferToken(wonIndex);
    } catch (e) {
      this.setState({ message: "Bu işlemi yapmak için yetkili değilsiniz" });
      console.log(e);
    }
  };

  getZeroVotes = async () => {
    await this.getSilver();
    await this.getGold();
    await this.getDiamond();
    await this.getPlatinum();
  };

  transferToken = async (wonIndex) => {
    let decimals = await token.methods.decimals().call();
    decimals++;
    let power = Math.pow(10, decimals);
    let value = (4 - wonIndex) * power;
    await token.methods.transfer(this.props.developerAddress, value).send({
      from: this.state.address,
      gasLimit: "100000",
    });
    decimals--;
    value /= Math.pow(10, decimals);
    this.setState({
      message: "Developera " + value + " MiEToken Transfer edildi",
    });
  };

  getVotes = async () => {
    await this.getSilver();
    await this.getGold();
    await this.getDiamond();
    await this.getPlatinum();
  };

  renderVoteButton = (text, func) => {
    return (
      <th>
        <div className="btnWrapper">
          <button className="button6" onClick={func}>
            {text} Pakete Oy Ver
          </button>
        </div>
      </th>
    );
  };

  renderVoteCount = (count) => {
    return (
      <td className="td">
        <h3>{count}</h3>
      </td>
    );
  };

  starList = [
    {
      src: silver,
      text: "10 Token",
    },
    {
      src: gold,
      text: "20 Token",
    },
    {
      src: diamond,
      text: "30 Token",
    },
    {
      src: platinum,
      text: "40 Token",
    },
  ];

  voteFuncList = [
    this.voteSilver,
    this.voteGold,
    this.voteDiamond,
    this.votePlatinum,
  ];

  render() {
    this.initState();
    return (
      <>
        <ConnectWalletButtonRow>
          <CupertinoButton
            style={{
              height: 70,
              width: 222,
              marginTop: 135,
              backgroundColor: "#FFCC00",
            }}
            caption={this.state.connectWalletText}
            onClick={this.connectWallet}
          ></CupertinoButton>
          <HeaderColumn>
            <Header>Oylama Ekranı</Header>
            <StarStackRow>
              {this.starList.map((starItem, index) => {
                return (
                  <StarStack key={index}>
                    <StarImage
                      src={starItem.src}
                      resizeMode="contain"
                    ></StarImage>
                    <Frame>
                      <Text>{starItem.text}</Text>
                    </Frame>
                  </StarStack>
                );
              })}
            </StarStackRow>
          </HeaderColumn>
          <VoteColumn>
            <VoteImg src={voteImg}></VoteImg>
            <MessageArea>
              <Message>{this.state.message}</Message>
            </MessageArea>
          </VoteColumn>
        </ConnectWalletButtonRow>
        <VoteButtonRow>
          {this.voteFuncList.map((voteFunc, index) => {
            return (
              <CupertinoButton
                key={index}
                style={{
                  height: 50,
                  width: 200,
                  marginLeft: index > 0 ? 40 : 0,
                  marginTop: 5,
                  backgroundColor: "#4CD964",
                }}
                caption="Oy Ver"
                onClick={voteFunc}
              ></CupertinoButton>
            );
          })}
        </VoteButtonRow>
        <VoteCountRow>
          {Array(4)
            .fill()
            .map((item, index) => {
              let voteCount;
              switch (index) {
                case 0:
                  voteCount = this.state.silver;
                  break;
                case 1:
                  voteCount = this.state.gold;
                  break;
                case 2:
                  voteCount = this.state.diamond;
                  break;
                default:
                  voteCount = this.state.platinum;
              }
              return (
                <VoteCountStack
                  key={index}
                  style={{
                    marginLeft: index > 0 ? 70 : 0,
                  }}
                >
                  <VoteCountContainer>
                    <VoteCountText>{voteCount}</VoteCountText>
                  </VoteCountContainer>
                </VoteCountStack>
              );
            })}
        </VoteCountRow>
        <CupertinoButton
          style={{
            height: 50,
            width: 500,
            marginTop: 50,
            marginLeft: 410,
            backgroundColor: "#5856D6",
          }}
          caption="Oylamayı Sonuçlandır"
          onClick={() => this.finalizeVote()}
        ></CupertinoButton>
      </>
    );
  }
}

export default VoteFunc;
